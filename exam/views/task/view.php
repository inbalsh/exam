<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
         //   'urgency',
        [ // בשביל להציג בדף הצפייה של כל פוסט
                'label' => 'Urgency',
                'value' => $model->urgency1->name
            ],
            'created_at',
       
            'updated_at',
      
        //    'created_by',

        [    // הופך את שם הכותב לקישור שמפנה לדף ביוזרס                  
                'label' => 'Author',
				'format' => 'html',
				'value' => Html::a($model->author1->name, 
					['user/view', 'id' => $model->author1->id]), 
           ],
/*
           [
                'label' => 'Author',
               'value' => $model->author1->created_by
            ],
*/
            'updated_by',
        ],
    ]) ?>

</div>
