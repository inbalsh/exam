<?php

use yii\db\Migration;

/**
 * Class m180624_085114_init_rbac
 */
class m180624_085114_init_rbac extends Migration
{
 /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $manager = $auth->createRole('manager');
        $auth->add($manager);
  
        $employee = $auth->createRole('employee');
        $auth->add($employee);


        $auth->addChild($manager, $employee);
  

        $createTask = $auth->createPermission('createTask');
        $auth->add($createTask);
  
        $viewTask = $auth->createPermission('viewTask');
        $auth->add($viewTask);                    
        

        $updateUser = $auth->createPermission('updateUser');
        $auth->add($updateUser);                    
        
        $viewUser = $auth->createPermission('viewUser');
        $auth->add($viewUser);                    
        
        $updateTask = $auth->createPermission('updateTask');
        $auth->add($updateTask);  

        $deleteTask = $auth->createPermission('deleteTask');
        $auth->add($deleteTask);  

        $deleteUser = $auth->createPermission('deleteUser');
        $auth->add($deleteUser);  

        $updateOwnUser = $auth->createPermission('updateOwnUser');
  
        $rule = new \app\rbac\EmployeeRule;
        $auth->add($rule);
                
        $updateOwnUser->ruleName = $rule->name;                
        $auth->add($updateOwnUser);                 
                                  
                
        $auth->addChild($manager, $updateUser);
        $auth->addChild($manager, $updateTask); 
        $auth->addChild($manager, $deleteTask); 
        $auth->addChild($employee, $viewTask);
        $auth->addChild($employee, $createTask);
        $auth->addChild($employee, $viewUser);
        $auth->addChild($updateOwnUser, $updateUser);
        $auth->addChild($employee, $updateOwnUser);
      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_072118_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_072118_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
