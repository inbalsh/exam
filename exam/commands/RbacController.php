<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use \app\rbac\EmployeeRule;


class RbacController extends Controller 
{
    public function actionInit() 
    {
        $auth = Yii::$app->authManager;
        $rule = new EmployeeRule;
        $auth->add($rule); 
    }
}