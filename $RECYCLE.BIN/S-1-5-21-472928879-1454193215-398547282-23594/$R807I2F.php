<?php

use yii\db\Migration;

/**
 * Class m180624_054840_add_values_to_urgency_table
 */
class m180624_054840_add_values_to_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        DB::table('urgency')->insert([
            ['name' => 'low'],
            ['name' => 'normal'],
            ['name' => 'critical']
      
        ]);
    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_054840_add_values_to_urgency_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_054840_add_values_to_urgency_table cannot be reverted.\n";

        return false;
    }
    */
}
